﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;



public class HealthCanvasContainer : MonoBehaviour
    {
        public List<BodyUIRefs> bodyParts;
    public Color fullHealth = Color.green;
    public Color lowHealth = Color.red;

    internal void UpdateState(BodyParts_UI bodyPartType, float currentHP, float maxHP)
    {

        BodyUIRefs currentPart = bodyParts.Find(item => (int)item.type == (int)bodyPartType);
       // if(currentPart!=null)
        {
            var lerpValue = currentHP / maxHP;
            var currentColor = Color.Lerp(lowHealth, fullHealth, currentHP / maxHP);
           // Debug.Log("lerpValue - " + lerpValue.ToString() + " ; currentHP - " + currentHP.ToString() + " ; maxHP - " + maxHP.ToString());
            currentPart.image.color = currentColor;

        }
        
    }
}

    [System.Serializable]
    public class BodyUIRefs
    {
        public Image image;
        public BodyParts_UI type;
    }

    public enum BodyParts_UI
    {
        hips,
        spine,
        chest,
        head,
        leftUpperArm,
        leftLowerArm,
        leftHand,
        rightUpperArm,
        rightLowerArm,
        rightHand,
        leftUpperLeg,
        leftLowerLeg,
        leftFoot,
        rightUpperLeg,
        rightLowerLeg,
        rightFoot
    }

