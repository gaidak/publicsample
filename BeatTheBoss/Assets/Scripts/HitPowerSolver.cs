﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HitPowerSolver : MonoBehaviour
{

    private float currentHP;
    private float maxHP;
    private float maxBreakForce;
    public Text partUI;

    internal BodyParts_UI bodyPartType;
    internal HealthCanvasContainer puppetPartContainer;

    //private void OnCollisionEnter(Collision collision)
    //{
    //    FindHitPower(collision, this.GetComponent<Rigidbody>());

    //}

    void FindHitPower(Collision collision, Rigidbody rigidbody)
    {
        float power = Mathf.Abs(Vector3.Dot(collision.contacts[0].normal, collision.relativeVelocity) * rigidbody.mass);
        float power2 = 0;
        Vector3 impulseVector = collision.impulse;

        float power3 = Mathf.Abs(collision.impulse.magnitude);
        if (collision.gameObject.GetComponent<Rigidbody>() != null)
        {
            power2 = Mathf.Abs(Vector3.Dot(collision.contacts[0].normal, collision.relativeVelocity) * collision.gameObject.GetComponent<Rigidbody>().mass);
        }
        //if (Mathf.Abs(power) > 1 || Mathf.Abs(power2) > 1)
        //{

        //}

        if ((power2 > 2 || power > 2) && power3 > 0)
        {
            Debug.Log("| Power - " + power + " | Power2 - " + power2 + " | ImpulseMagnitude - " + power3 + " ___ from " + collision.gameObject.name + " - to " + this.gameObject.name);
            // this.GetComponent<Joint>().breakForce = 0;
            //currentHP -= power2 == 0 ? power : power2;
            ApplyDamage(Mathf.RoundToInt(Mathf.Max(power, power2)));
        }
    }

    private void UpdateUI()
    {
        if (partUI != null)
        {
            partUI.text = this.name + Environment.NewLine + currentHP + "/" + maxHP;
        }
        if (puppetPartContainer != null)
        {
            puppetPartContainer.UpdateState(bodyPartType, currentHP, maxHP);
        }
    }

    public void SetupPart(float HP, float breakForce, bool resetMax = true)
    {
        currentHP = HP;
        if (resetMax)
        {
            maxHP = HP;
        }
        maxBreakForce = breakForce;
        if (this.GetComponent<Joint>() && breakForce == 0)
        {
            this.GetComponent<Joint>().breakForce = 0;
        }
        UpdateUI();
    }

    public static float KineticEnergy(Rigidbody rb)
    {
        // mass in kg, velocity in meters per second, result is joules
        return 0.5f * rb.mass * Mathf.Pow(rb.velocity.magnitude, 2);
    }

    void OnJointBreak()
    {
        currentHP = 0;
        UpdateUI();
    }

    public void ApplyDamage(int damage)
    {
        if (currentHP > 0 && damage > 0)
        {
            Debug.Log("Damge " + bodyPartType.ToString() + " - " + damage);
            currentHP -= damage;
            currentHP = Mathf.Clamp(currentHP, 0, currentHP);
            if (this.GetComponent<Joint>())
            {
                if (maxBreakForce > 0)
                {

                    this.GetComponent<Joint>().breakForce = currentHP > 0 ? Mathf.Infinity : 0;
                }
                else
                {
                    this.GetComponent<Joint>().breakForce = Mathf.Infinity;
                }
            }
            else
            {
                currentHP = 0;
            }
            UpdateUI();
        }
    }

    public float GetCurrentHealthPersent()
    {
        return currentHP / maxHP;
    }

    public float GetCurrentTearOffForce()
    {
        return (currentHP / maxHP) * ((maxBreakForce > 0) ? maxBreakForce : Int32.MaxValue);
    }
}

