﻿using ItSeez3D.AvatarSdk.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class KostilRefs : MonoBehaviour
{
    public BodyAttachment attachScript;
    public SkinnedMeshRenderer headSkin;
   // public Mesh headMesh;

    public SkinnedMeshRenderer hearSkin;
   // public Mesh hearMesh;

    public List<HeadInfo> heads;
    // Start is called before the first frame update
    public void Awake()
    {

        int rand = 2;

        SetNewHead(rand);
    }

    public void SetNewHead(int headIndex)
    {
        headSkin.sharedMesh = heads[headIndex].headMesh;
        headSkin.sharedMaterial.mainTexture = heads[headIndex].headTexture;


        if (heads[headIndex].hearMesh != null)
        {
            hearSkin.enabled = true;
            hearSkin.sharedMesh = heads[headIndex].hearMesh;
            hearSkin.sharedMaterial.mainTexture = heads[headIndex].hearTexture;
        }
        else
        {
            hearSkin.enabled = false;
        }

        attachScript.Start();
    }
}

[System.Serializable]
public class HeadInfo
{
    public string Name;
    
    public Mesh headMesh;
    public Texture headTexture;

    public Mesh hearMesh;
    public Texture hearTexture;
}