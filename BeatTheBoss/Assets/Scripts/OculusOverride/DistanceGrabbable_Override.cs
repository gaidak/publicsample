﻿using System;
using UnityEngine;
using OVRTouchSample;
using RootMotion.Dynamics;
using System.Collections.Generic;

namespace OculusSampleFramework
{
    public class DistanceGrabbable_Override : OVRGrabbable
    {
        public string m_materialColorField;
        public float respawnTime = 10f;

        GrabbableCrosshair m_crosshair;
        GrabManager_Override m_crosshairManager;
        Renderer m_renderer;
        MaterialPropertyBlock m_mpb;

        public float _throwFactor = 1;
        public float _rotationFactor = 1;
        public float hitDamageFactor = 1;

        private Quaternion prevRotation, currentRottation;
        private Vector3 lastTrackedVelocity;
        protected int startLayer;

        public bool InRange
        {
            get { return m_inRange; }
            set
            {
                m_inRange = value;
                RefreshCrosshair();
            }
        }
        bool m_inRange;

        public bool Targeted
        {
            get { return m_targeted; }
            set
            {
                m_targeted = value;
                RefreshCrosshair();
            }
        }
        bool m_targeted;

        [SerializeField]
        private int itemDurability = 1000;

        private float respawnTimer;
        private bool once;
        private Vector3 startPos;
        private Quaternion startRot;
        private Rigidbody myRigidbody;
        public bool needComeBack;
        private bool canCollideAgain;
        List<Collider> alreadyTouched;


        protected override void Start()
        {
            alreadyTouched = new List<Collider>();
            base.Start();
            m_crosshair = gameObject.GetComponentInChildren<GrabbableCrosshair>();
            m_renderer = gameObject.GetComponent<Renderer>();
            m_crosshairManager = FindObjectOfType<GrabManager_Override>();
            m_mpb = new MaterialPropertyBlock();
            RefreshCrosshair();
            if (!string.IsNullOrEmpty(m_materialColorField))
            {
                m_mpb.SetColor(m_materialColorField, Color.white);
                m_renderer.SetPropertyBlock(m_mpb);
            }

            myRigidbody = this.GetComponent<Rigidbody>();

            needComeBack = true;
            canCollideAgain = true;
            startLayer = this.gameObject.layer;
        }

        void RefreshCrosshair()
        {
            if (m_crosshair)
            {
                if (isGrabbed) m_crosshair.SetState(GrabbableCrosshair.CrosshairState.Disabled);
                else if (!InRange) m_crosshair.SetState(GrabbableCrosshair.CrosshairState.Disabled);
                else m_crosshair.SetState(Targeted ? GrabbableCrosshair.CrosshairState.Targeted : GrabbableCrosshair.CrosshairState.Enabled);
            }
            if (m_materialColorField != null && !string.IsNullOrEmpty(m_materialColorField))
            {
                m_renderer.GetPropertyBlock(m_mpb);
                if (isGrabbed || !InRange) m_mpb.SetColor(m_materialColorField, Color.white);
                else if (Targeted) m_mpb.SetColor(m_materialColorField, m_crosshairManager.OutlineColorHighlighted);
                else m_mpb.SetColor(m_materialColorField, m_crosshairManager.OutlineColorInRange);
                m_renderer.SetPropertyBlock(m_mpb);
            }
        }

        public void SetColor(Color focusColor)
        {
            if (!string.IsNullOrEmpty(m_materialColorField))
            {
                m_mpb.SetColor(m_materialColorField, focusColor);
                m_renderer.SetPropertyBlock(m_mpb);
            }
        }

        public void ClearColor()
        {
            if (!string.IsNullOrEmpty(m_materialColorField))
            {
                m_mpb.SetColor(m_materialColorField, Color.white);
                m_renderer.SetPropertyBlock(m_mpb);
            }
        }

        override public void GrabBegin(OVRGrabber hand, Collider grabPoint)
        {
            if (GetComponent<FixedJoint>() != null)
            {
                Destroy(GetComponent<FixedJoint>());
            }
            //if (GetComponent<StabKnife>() != null)
            //{
            //    GetComponent<StabKnife>().GrabProcessing();// = true;
            //}
            base.GrabBegin(hand, grabPoint);

            needComeBack = false;
        }

        override public void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
        {
            lastTrackedVelocity = linearVelocity;
            base.GrabEnd(linearVelocity
                * _throwFactor
                , angularVelocity * _rotationFactor);
            needComeBack = true;
        }

        protected virtual void Update()
        {

            if (!needComeBack)
            {
                respawnTimer = 0;
            }
            else
            {
                if (myRigidbody.velocity.sqrMagnitude <= 0.01f && !once)
                {
                    once = true;
                    startPos = transform.position;
                    startRot = transform.rotation;
                }
                if (Vector3.SqrMagnitude(transform.position - startPos) > 1)
                {
                    respawnTimer += Time.deltaTime;
                }
                if (respawnTimer > respawnTime)
                {
                    respawnTimer = 0;
                    this.transform.position = startPos;
                    this.transform.rotation = startRot;
                    myRigidbody.velocity = Vector3.zero;
                    myRigidbody.angularVelocity = Vector3.zero;
                }
            }
        }

        virtual public void OnCollisionEnter(Collision collision)
        {
            if (canCollideAgain && hitDamageFactor > 0)
            {
                alreadyTouched.Clear();
                int random = UnityEngine.Random.Range(0, 1000000000);
                HitPowerSolver hitComponent;
                for (int i = 0; i < collision.contactCount; i++)
                {
                    if (!alreadyTouched.Contains(collision.contacts[i].otherCollider))
                    {
                        alreadyTouched.Add(collision.contacts[i].otherCollider);
                        hitComponent = collision.contacts[i].otherCollider.GetComponent<HitPowerSolver>();

                        if (hitComponent != null && collision.relativeVelocity.magnitude > 3 && collision.impulse.magnitude > 1)
                        {
                            // Debug.Log("relativeVel - " + collision.relativeVelocity.magnitude + " ; impulse - " + collision.impulse.magnitude + " ; HitPower - " + GetHitPower() +" ; Name - " + hitComponent.gameObject.name + " ; other - " + collision.contacts[0].otherCollider.name + " ; this - " + collision.contacts[0].thisCollider.name);
                            var damage = (int)(hitDamageFactor * GetHitPower() * 10f);
                            MakeDamage(hitComponent, damage);
                        }
                    }
                }
            }

        }

        private float GetHitPower()
        {
            if (isGrabbed)
            {
                return OVRInput.GetLocalControllerVelocity(((OVRGrabber_Override) grabbedBy).GetControllerType()).magnitude;
            }
            else
            {
                return lastTrackedVelocity.magnitude;
            }
        }

        virtual public void MakeDamage(HitPowerSolver hitObject, int damage)
        {
            hitObject.ApplyDamage(damage);
            //update item durability 
            itemDurability -= damage;
            if (itemDurability <= 0)
            {
                if (grabbedBy != null)
                {
                    grabbedBy.ForceRelease(this);
                }
                //changeLayer. cant grab broken item
                this.gameObject.layer = 5;

                needComeBack = false;
                //just for demo - respawr;
                Invoke("ResetItem", 5f);
            }

        }

        void ResetItem()
        {
            needComeBack = true;
            itemDurability = 1000;
            m_grabbedBy = null;
            this.gameObject.layer = startLayer;
            //spawn point
            respawnTimer = 0;
            this.transform.position = startPos;
            this.transform.rotation = startRot;
            myRigidbody.velocity = Vector3.zero;
            myRigidbody.angularVelocity = Vector3.zero;
        }
    }
}
