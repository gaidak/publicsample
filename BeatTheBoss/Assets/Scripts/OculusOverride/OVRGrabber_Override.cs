﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Allows grabbing and throwing of objects with the OVRGrabbable component on them.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class OVRGrabber_Override : OVRGrabber
{

    public Transform forwardDirection = null;

    public OVRInput.Controller GetControllerType()
    {
        return m_controller;
    }

    protected new void CheckForGrabOrRelease(float prevFlex)
    {
        if ((m_prevFlex >= grabBegin) && (prevFlex < grabBegin))
        {
            GrabBegin();
            if (m_grabbedObj == null)
            {
                DynamycAnchor(true);
            }
        }
        else if ((m_prevFlex <= grabEnd) && (prevFlex > grabEnd))
        {
            GrabEnd();
            if (m_grabbedObj == null)
            {
                DynamycAnchor(false);
            }
        }
    }

    private void DynamycAnchor(bool on_off)
    {
        if (dynamycAnchorRef != null)
        {
            dynamycAnchorRef.SetActive(on_off);
        }
    }

   

    protected new void GrabEnd()
    {
        if (m_grabbedObj != null)
        {
            OVRPose localPose = new OVRPose { position = OVRInput.GetLocalControllerPosition(m_controller), orientation = OVRInput.GetLocalControllerRotation(m_controller) };
            OVRPose offsetPose = new OVRPose { position = m_anchorOffsetPosition, orientation = m_anchorOffsetRotation };
            localPose = localPose * offsetPose;

            OVRPose trackingSpace = transform.ToOVRPose() * localPose.Inverse();
            Vector3 linearVelocity = trackingSpace.orientation * OVRInput.GetLocalControllerVelocity(m_controller);

            Vector3 angularVelocity = forwardDirection.rotation * OVRInput.GetLocalControllerAngularVelocity(m_controller);

            //Debug.Log("angVel - " + "(" + angularVelocity.x + "; " + angularVelocity.y + "; " + angularVelocity.z + ";" + ")" + " ; angVel2 - " + "(" + angularVelocity2.x + "; " + angularVelocity2.y + "; " + angularVelocity2.z + ";" + ")");


            GrabbableRelease(linearVelocity, angularVelocity);
        }

        // Re-enable grab volumes to allow overlap events
        GrabVolumeEnable(true);
    }


}
