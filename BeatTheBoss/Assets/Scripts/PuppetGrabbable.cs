﻿using OculusSampleFramework;
using RootMotion.Dynamics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuppetGrabbable : DistanceGrabbable_Override
{
    public float neededCollisionDistance = 0.05f;
    public Joint thisBodyPartJoint;
    FixedJoint tempJoint;
    private HitPowerSolver hitPowerSolver;
    [SerializeField]
    private int tearOffForce = 200;
    private float timer;

    protected override void Start()
    {
        base.Start();
        needComeBack = false;
        thisBodyPartJoint = this.GetComponent<Joint>();
      
    }

    public override void GrabBegin(OVRGrabber hand, Collider grabPoint)
    {
        if (tempJoint != null)
        {
            Destroy(GetComponent<FixedJoint>());
        }
        //base.GrabBegin(hand, grabPoint);
        m_grabbedBy = hand;
        m_grabbedCollider = grabPoint;

        //gameObject.GetComponent<Rigidbody>().isKinematic = true;

        tempJoint = hand.gameObject.AddComponent<FixedJoint>();

        tempJoint.connectedBody = gameObject.GetComponent<Rigidbody>();

        hitPowerSolver = this.GetComponent<HitPowerSolver>();

        tempJoint.breakForce = 1500;
        tempJoint.breakTorque = 1500;

     
        needComeBack = false;
    }

    public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        // base.GrabEnd(linearVelocity, angularVelocity, lineralAcceleration, angularAcceleration);
        m_grabbedBy = null;
        m_grabbedCollider = null;
        needComeBack = false;

        if (tempJoint != null)
        {
            Destroy(tempJoint);
        }
    }

     protected override void Update()
    {
        if(tempJoint!=null && hitPowerSolver!=null && thisBodyPartJoint!=null)
        {
            timer += Time.deltaTime;
            if (timer > 0.333f)
            {
                timer = 0;
                Debug.Log("CurrentTearOffForce = " + hitPowerSolver.GetCurrentTearOffForce() + " ; currentForce = " + tempJoint.currentForce.magnitude);
            }
            if (hitPowerSolver.GetCurrentTearOffForce()<= tempJoint.currentForce.magnitude && tempJoint.currentForce.magnitude > tearOffForce)
            {
                Debug.LogError(" BREAK - CurrentTearOffForce = " + hitPowerSolver.GetCurrentTearOffForce() + " ; currentForce = " + tempJoint.currentForce.magnitude);
                hitPowerSolver.SetupPart(0, 0, false);
                hitPowerSolver.enabled = false;
            }
        }
    }
}
