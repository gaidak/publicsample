﻿using RootMotion.Dynamics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuppetHealthUI : MonoBehaviour
{
    public GameObject layoutRoot;
    public List<GameObject> partsUI;
    public GameObject partUIprefab;

    public HealthCanvasContainer puppetPartContainer;
    public List<LimbPart> limbRefs;
    // Start is called before the first frame update
    private void Start()
    {
        // BipedRagdollReferences r = BipedRagdollReferences.FromAvatar(instance.GetComponent<Animator>());

        for (int i = 0; i < limbRefs.Count; i++)
        {
            HitPowerSolver currentPart = limbRefs[i].muscle.gameObject.AddComponent<HitPowerSolver>();
            currentPart.bodyPartType = limbRefs[i].type;
            currentPart.puppetPartContainer = puppetPartContainer;
           

            GameObject partUI = Instantiate(partUIprefab, layoutRoot.transform);
            partsUI.Add(partUI);
            currentPart.partUI = partUI.GetComponentInChildren<Text>();

            currentPart.SetupPart(limbRefs[i].maxHP, limbRefs[i].breakForce);
        }
     

    }
}

[System.Serializable]
public class LimbPart
{
    public Joint muscle;
    public BodyParts_UI type;
    public float maxHP;
    public float breakForce;
}