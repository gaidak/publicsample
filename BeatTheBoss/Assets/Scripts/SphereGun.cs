﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereGun : MonoBehaviour
{
    [SerializeField]
    private float GunPower = 5;
    [SerializeField]
    private float BulletMass = 1;
    [SerializeField]
    private Renderer boxRender;

    private GameObject[] bulletsPool;
    private Rigidbody[] bulletsBodyRef;
    private int currentBulletIndex;
    private float additionalMult;
    private float maxAdditionalMult = 5;
    private float prewAdditionalMult;

    // Start is called before the first frame update
    void Awake()
    {
        additionalMult = 1;
        bulletsPool = new GameObject[10];
        bulletsBodyRef = new Rigidbody[10];
        for (int i = 0; i < bulletsPool.Length; i++)
        {
            bulletsPool[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            bulletsPool[i].name = "Sphere_" + (i + 1);
            bulletsPool[i].transform.position = this.transform.position;
            bulletsPool[i].transform.localScale = Vector3.one * 0.1f;
            Rigidbody sphereRigidbody = bulletsPool[i].AddComponent<Rigidbody>();
            sphereRigidbody.mass = BulletMass;
            sphereRigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            bulletsBodyRef[i] = sphereRigidbody;
            bulletsPool[i].SetActive(false);
        }
        if(boxRender==null)
        {
            boxRender = this.transform.Find("Cube").GetComponent<Renderer>();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.G) || OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.Get(OVRInput.Button.PrimaryHandTrigger))
        {
            AccumulatePower();
        }
        if (Input.GetKeyUp(KeyCode.G) || OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger))
        {
            Fire();
            additionalMult = 1;
        }

        if(additionalMult!= prewAdditionalMult)
        {
            if (boxRender != null)
            {
                boxRender.material.SetColor( "_Color",Color.Lerp(Color.white, Color.black, additionalMult / maxAdditionalMult));
            }
        }

    }

    private void AccumulatePower()
    {
       

      if (additionalMult < maxAdditionalMult)
        {
            additionalMult += Time.deltaTime*3;             
        }
    }

    private void Fire()
    {
       

        currentBulletIndex++;
        currentBulletIndex = currentBulletIndex % (bulletsPool.Length);

        var nextBulletIndex = currentBulletIndex+1;
        nextBulletIndex = currentBulletIndex % (bulletsPool.Length);
        bulletsPool[nextBulletIndex].SetActive(false);
        bulletsBodyRef[nextBulletIndex].velocity = Vector3.zero;

      //  Debug.Log(currentBulletIndex);
        bulletsPool[currentBulletIndex].SetActive(true);
        bulletsPool[currentBulletIndex].transform.position = this.transform.position;

        bulletsPool[currentBulletIndex].transform.localScale = Vector3.one * 0.02f*additionalMult;

        bulletsBodyRef[currentBulletIndex].mass = BulletMass *additionalMult;
        bulletsBodyRef[currentBulletIndex].AddForce(this.transform.forward * GunPower * additionalMult, ForceMode.Impulse);
    }
}
