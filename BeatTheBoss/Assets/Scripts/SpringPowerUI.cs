﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpringPowerUI : MonoBehaviour
{
    public SpringJoint[] joints;
    public Text textUI;
    public Slider slider;
    // Start is called before the first frame update


    public void ChangeValue_Slider()
    {
        for (int i = 0; i < joints.Length; i++)
        {
            joints[i].spring = slider.value;
            textUI.text = "Spring power = " + slider.value;
        }
    }
}
