﻿using OculusSampleFramework;
using RootMotion.Dynamics;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StabObject : DistanceGrabbable_Override

{
   
    public List<Collider> sharpEnds;

    [Tooltip ("Off this colliders when object inside bodyPart")]
    public List<Collider> edges;
   // public Collider bladeTrigger;
   
    public float stabMagnitude = 0;
    public float stabFactor01 = 0.8f;
    public float diggingSize = 0.5f;
    public int stabDamage = 50;



    public bool NeedCorrectAngle = false;
    public bool stabFromHand = false;

    [Tooltip ("Raycast from sharpEdges. layers for detect puppet and other stabbable objects))")]
    public LayerMask mask;

    private Rigidbody _rigidbody;
    private List<Collider> colliders;
    private  DistanceGrabbable_Override grabbedObject;
    //   private int startLayer;
    private int stabLayer = 17;
    private bool grabProcessing = false;
    private Rigidbody lastStabObject;
    private Collider connectedCollider;
    [SerializeField]
    private bool useLikeKnife = false;
    private bool stabbed;

    protected override void Start()
    {
        base.Start();
        grabbedObject = this;
        _rigidbody = this.GetComponent<Rigidbody>();
        _rigidbody.maxAngularVelocity *= 3;
        startLayer = _rigidbody.gameObject.layer;
        colliders = gameObject.GetComponentsInChildren<Collider>().ToList();
    }

    protected override void Update()
    {
        base.Update();
    }

    override public  void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
        float power3 = Mathf.Abs(collision.impulse.magnitude);
        if (power3 > 0 || (stabFromHand && grabbedObject.grabbedBy != null))
        {
            foreach (ContactPoint contactPoint in collision.contacts)
            {
                // are we hitting with the pointy bit?
                if (sharpEnds.Contains(contactPoint.thisCollider) && ((!stabFromHand && !grabbedObject.isGrabbed) || (stabFromHand && !grabProcessing)))
                {

                    Transform sharpEnd = contactPoint.thisCollider.transform;

                    Debug.Log("collision. Move - " + Vector3.Dot(_rigidbody.velocity.normalized, sharpEnd.forward) + " ; angle - " + Vector3.Dot(contactPoint.normal, sharpEnd.forward) + " ; Magnitude - " + _rigidbody.velocity.magnitude + " ; impulse - " + power3);
                    // decent angle?
                    //can stab and damage FROM HAND (grabbedBy != null) only if "knife" type and correct movement vector
                    if ((!useLikeKnife || grabbedObject.grabbedBy == null) || (useLikeKnife && Vector3.Dot(_rigidbody.velocity.normalized, sharpEnd.forward) >= (1.0f * stabFactor01)))
                    {
                        if (!NeedCorrectAngle || Vector3.Dot(contactPoint.normal, sharpEnd.forward) <= (-1.0f * stabFactor01))
                        {
                            // is it going fast enough?
                            if (_rigidbody.velocity.magnitude >= stabMagnitude)
                            {
                                if (grabbedObject.grabbedBy != null)
                                {
                                    //  grabbedObject.grabbedBy.ForceRelease(grabbedObject);
                                }
                                // embed it
                                if (contactPoint.otherCollider.transform.GetComponent<Rigidbody>() != null)
                                {
                                   
                                    //  Vector3 newPosition = rigidbody.transform.position - contactPoint.normal * diggingSize;
                                    Vector3 newPosition = _rigidbody.transform.position + _rigidbody.velocity.normalized * diggingSize * _rigidbody.gameObject.transform.lossyScale.x;

                                    Transform parent = sharpEnd.parent;


                                    Vector3 offset = parent.position - sharpEnd.position;
                                    Ray ray = new Ray(sharpEnd.position, sharpEnd.forward);
                                    RaycastHit hit = new RaycastHit();
                                    if (Physics.Raycast(ray, out hit, 0.1f, mask))
                                    {
                                        newPosition = (hit.point + sharpEnd.forward * diggingSize) + offset;

                                        //////debug part
                                        //Debug.Log("hit.point - " + "(" + hit.point.x + ";" + hit.point.y + ";" + hit.point.z + ";" + ")" + " ; Object - " + hit.collider.gameObject.name);

                                        GameObject point1 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                                        point1.name = "point1";
                                        point1.transform.position = hit.point;
                                        point1.transform.localScale = Vector3.one * 0.02f;
                                        point1.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
                                        point1.GetComponent<Collider>().enabled = false;
                                        point1.AddComponent<AutoDestroy>();

                                        GameObject point2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                                        point2.name = "point2";
                                        point2.transform.position = contactPoint.point;
                                        point2.transform.localScale = Vector3.one * 0.01f;
                                        point2.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
                                        point2.GetComponent<Collider>().enabled = false;
                                        point2.AddComponent<AutoDestroy>();



                                        //Debug.Log("contactPoint - " + "(" + contactPoint.point.x + ";" + contactPoint.point.y + ";" + contactPoint.point.z + ";" + ")" + " ; NewPos - " + "(" + newPosition.x + ";" + newPosition.y + ";" + newPosition.z + ";" + ")");

                                        //////end debug sphere part
                                        if (useLikeKnife && grabbedObject.grabbedBy != null)
                                        {
                                            SetLayer(stabLayer, true);
                                        }
                                        else
                                        {
                                            SetLayer(stabLayer);
                                            _rigidbody.transform.position = newPosition;
                                        }



                                        // Debug.LogError("Knife release. Joint to - " + contactPoint.otherCollider.transform.name);
                                        if (grabbedObject.grabbedBy != null)
                                        {
                                            lastStabObject = hit.collider.transform.GetComponent<Rigidbody>();
                                            connectedCollider = hit.collider;
                                        }
                                        else
                                        {
                                            lastStabObject = hit.collider.transform.GetComponent<Rigidbody>();
                                            connectedCollider = hit.collider;
                                            ConnectToStabPlace(hit.collider.transform.GetComponent<Rigidbody>());
                                        }



                                        if (hit.collider.transform.GetComponent<HitPowerSolver>())
                                        {
                                            MakeDamage(hit.collider.transform.GetComponent<HitPowerSolver>(), stabDamage);
                                        }
                                        break;
                                    }
                                }
                                else
                                {
                                    // Debug.LogError("Knife release. Parent to - " + contactPoint.otherCollider.transform.root.name);
                                    //  transform.SetParent(contactPoint.otherCollider.transform.root, true);
                                }



                                return;
                            }
                        }
                    }
                }
            }
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (lastStabObject != null && other.GetComponent<Rigidbody>() == lastStabObject && other == connectedCollider)
        {
            lastStabObject = null;
            connectedCollider = null;
            SetLayer(-1,true);
            Debug.LogError(" OnTriggerExit. Rigidbody  - " + other.GetComponent<Rigidbody>().gameObject.name);
        }
    }

    private void ConnectToStabPlace(Rigidbody connectedBody)
    {
        FixedJoint joint = this.gameObject.AddComponent<FixedJoint>();

        joint.connectedBody = connectedBody;
        joint.breakForce = 3000;
        joint.breakTorque = 3000;

        _rigidbody.velocity *= 0.1f;
        _rigidbody.isKinematic = false;

        _rigidbody.angularVelocity = Vector3.zero;
        stabbed = true;

        grabbedObject.needComeBack = false;
    }

    override public void MakeDamage(HitPowerSolver hitObject,int damage)
    {
        base.MakeDamage(hitObject, damage);
    }

    internal void GrabProcessing()
    {
        grabProcessing = true;
        Invoke("OffGrabb", 0.5f);
    }

    void OffGrabb()
    {
        grabProcessing = false;
    }

    public override void GrabBegin(OVRGrabber hand, Collider grabPoint)
    {
        base.GrabBegin(hand, grabPoint);
        _rigidbody.gameObject.layer = startLayer;
        SetLayer();
        GrabProcessing();
    }
 
    public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        base.GrabEnd(linearVelocity, angularVelocity);
        if (lastStabObject != null)
        {
            ConnectToStabPlace(lastStabObject);
        }
    }

    private void OnJointBreak(float breakForce)
    {
        stabbed = false;
        grabbedObject.needComeBack = true;
    }

    protected void SetLayer(int layer = -1, bool onlyEdges = false)
    {
        if (onlyEdges)
        {
            for (int i = 0; i < edges.Count; i++)
            {
                edges[i].gameObject.layer = layer == -1 ? startLayer : layer;
            }
        }
        else
        {
            for (int i = 0; i < colliders.Count; i++)
            {
                colliders[i].gameObject.layer = layer == -1 ? startLayer : layer;
            }
        }
    }
}
