﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Linq;

public class CollisionMatrixSaver 
{

    [MenuItem("Tools/CreateMatrixCollision")]
    static void CreateMatrix()
    {
        string fileName = Application.productName+"_CollisionMatrix" + ".txt";
        string targetFolder = "Assets/Resources/CollisionMatrix/";
        string filePath = targetFolder + fileName;
        if (!Directory.Exists(targetFolder))
        {
            Directory.CreateDirectory(targetFolder);
        }
        int layersCount = 32;
        int[] collisionMatrixData = new int[layersCount*layersCount];
        
        for (int i = 0; i < layersCount; i++)
        {
            for (int j = 0; j < layersCount; j++)
            {
                collisionMatrixData[j + i * layersCount] = (Physics.GetIgnoreLayerCollision(i,j)?1:0);               
            }
        }

        MatrixCollisionData myData = new MatrixCollisionData();

        myData.data = collisionMatrixData.ToList();
          
        string json = JsonUtility.ToJson(myData);

        File.WriteAllText(filePath, json);
    }

    [MenuItem("Tools/LoadMatrixCollision")]
    static void LoadMatrix()
    {
        string fileName = Application.productName + "_CollisionMatrix" +".txt";
        string targetFolder = "Assets/Resources/CollisionMatrix/";
        string filePath = targetFolder + fileName;

        if (File.Exists(filePath))
        {
            string json = File.ReadAllText(filePath);

            Debug.Log(json);
            MatrixCollisionData myData = JsonUtility.FromJson<MatrixCollisionData>(json);

            int layersCount =32;


            for (int i = 0; i < layersCount; i++)
            {
                for (int j = 0; j < layersCount; j++)
                {
                    bool ignore = myData.data[j + i * layersCount] == 1;
                    Physics.IgnoreLayerCollision(i, j, ignore);
                }
            }
        }
    }


    [System.Serializable]
   public class MatrixCollisionData
    {
        public List<int> data;
    }
}
