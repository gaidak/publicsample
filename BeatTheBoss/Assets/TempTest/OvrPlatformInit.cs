﻿using Oculus.Platform;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvrPlatformInit : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        try
        {
            Debug.Log("INIT OVR Start");
            Core.AsyncInitialize();
            // Debug.Log("INIT OVR End");
        }
        catch (UnityException e)
        {
            Debug.LogError("Platform failed to initialize due to exception.");
            Debug.LogError(e);
            // Immediately quit the application.

        }
    }


    public void OpenDeeplink()
    {
        Debug.Log("YEEEAAA, DEEPLINK");
        var options = new ApplicationOptions();
        options.SetDeeplinkMessage("desirium");
        // ulong app_id = 1093086134103708;
        ulong app_id = 1151852774921213;
        var callback = Oculus.Platform.Application.LaunchOtherApp(app_id, options);

    }
}
